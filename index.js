let students = [

	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"

];

console.log(students);


class Dog {

	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age;

	}

};

let dog1 = new Dog("Bantay","corgi",3);

console.log(dog1);

let person1 = {

	name: "Saitama",
	heroName: "One Punch Man",
	age: 30

};

console.log(person1);

// What array method can we use to add an item at the end of an array?
students.push("Henry");
console.log(students);

// What array method can we use to add an item at the start of an array?

students.unshift("Jeremiah");
console.log(students);

students.unshift("Christine");
console.log(students);

// What array method is the opposite of push()?

students.pop();
console.log(students);

// What array method is the opposite of unshift()?

students.shift();
console.log(students);

// What is the difference between splice() and slice()?
// .splice() allows to remove and add items from a starting index.
// .slice() copies a portion from a starting index and returns a new array from it.

// What is another kind of array method?
// Iterator methods loops over the items of an array.

// forEach() - loops over items in an array and repeats a user-defined function.
// map - loops over items in an array and repeats a user-defined function AND returns a new array.

// every() - loops and checks if all items satisfy a given condition and returns a boolean.

let arrNum = [15,25,50,20,10,11];

// check if every item in arrNum is divisible by 5:
let allDivisible;
arrNum.forEach(num => {

	if(num % 5 === 0){

		console.log(`${num} is divisble by 5.`);

	} else {

		allDivisible = false;

	};

	// can, forEach() return data that will tell us IF all numbers/items in our arrNum array is divisible by 5?

});

console.log(allDivisible);

// arrNum.pop();
arrNum.push(35);

let divisibleBy5 = arrNum.every(num => {

	// Every is able to return data.
	// When the function inside every() is able to return true, the loop will continue until all items are able to return true. Else, IF at least one item returns false, then the loop will stop there and every() will return false.

	console.log(num);
	return num % 5 === 0;

});

console.log(divisibleBy5);

// some() - loops and checks if at least one item satisfies a given condition and returns a boolean. It will return true if at least ONE item satisfies the condition, else if no item returns true/satisfies the condition, some will return false.

let someDivisibleBy5 = arrNum.some(num => {

	console.log(num);
	return num % 5 === 0;

});

console.log(someDivisibleBy5);

let someDivisibleBy11 = arrNum.some(num => {

	console.log(num);
	return num % 11 === 0;

});

console.log(someDivisibleBy11);

let someDivisibleBy6 = arrNum.some(num => {

	console.log(num);
	return num % 6 === 0;

});

console.log(someDivisibleBy6);

// QUIZ
/*
	1. How do you create arrays in JS?
		-enclosing the data in a array literal ("[ ])

	2. How do you access the first character of an array?
		-arr[0]

	3. How do you access the last character of an array?
		-arr[arr.length-1]

	4. What array method searches for, and returns the index of a given value in an array?
		a. This method returns -1 if given value is not found in the array.
		-indexOf()

	5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
		-forEach()

	6. What array method creates a new array with elements/data returned from a user-defined function?
		-map()

	7. What array method checks if all its elements satisfy a given condition?
		-every()

	8. What array method checks if atleast one of its elements satisfies a given condition?
		-some()

	9. True or False: array.splice() modifies a copy of the array, leaving the original unchaged.
		-true

	10. True or False. array.slice() copies elements from original array and return these as a new array.
		-true

	11. An Array is a primitive data type. True or False?
		-false

	12. What global JS object contains pre-defined methods and properties that are used for mathematical calcaulations?
		-Math

	13. True or False? The number of characters in a string can be determined using the .length property because strings are actually objects too.
		-false

	14. True or False? The methods of an Array are user-defined.
		-false

	15. True or False? We can use the .toString() on a boolean.
		-true 
*/

// FUNCTION CODING


/*
	Problem 1 - Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string. return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/

// Solution:
function addToEnd(array,name){

	if(typeof name !== "string"){

		return "error - can only add strings to an array";

	} else {

		array.push(name);
		return array;

	};

};

/*
	Problem 2 - Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

*/
// Solution:
function addToStart(array,name){

	if(typeof name !== "string"){

		return "error - can only add strings to an array";

	} else {

		array.unshift(name);
		return array;

	};

};

/*
	Problem 3 - Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message, "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arumgents when testing.
*/
// Solution:
function elementChecker(array,name){

	if(array.length === 0){

		return "error - passed in array is empty";

	} else {

		return array.includes(name);

	};

};

/*
	Problem 4 - Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements mus be strings". Otherwise, return the sorted array. Use the students array to test.
*/
// Solution:
function stringLengthSorter(array){

	let isArrayStringOnly = array.every(element => {

		return typeof element === "string";

	});

	// console.log(isArrayStringOnly);

	if(isArrayStringOnly){

		const ascending = array.sort((a,b) => a.length - b.length);

		return ascending;	

	} else {

		return "error - all array elements must be strings";

	};

};
